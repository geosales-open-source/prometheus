package br.com.softsite.prometheus;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.HashMap;

import org.junit.Test;

import br.com.softsite.prometheus.lifecycle.CycleBuilder;
import br.com.softsite.prometheus.lifecycle.CycleCache;
import br.com.softsite.prometheus.lifecycle.ValidationCycle;

public class RegisterCacheTest {

	@Test
	public void cacheApiAvailable() {
		ValidationCycle<StringBuilder> cycle = new CycleBuilder<StringBuilder>()
				.registerCache("a", (String s) -> s.toUpperCase())
				.registerCache("b", (String s, CycleCache c) -> s + c.getCache("a", s))
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "1a3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "2b3");
					s.append(x);
				})
				.generateCycle();
		StringBuilder b = new StringBuilder();
		cycle.validateAndPersist(b);
		assertEquals("1A32b32B3", b.toString());
	}

	@Test
	public void cacheDoesNotRecompute() {
		HashMap<String, Integer> contagemChaveA = new HashMap<>();
		HashMap<String, Integer> contagemChaveB = new HashMap<>();

		ValidationCycle<StringBuilder> cycle = new CycleBuilder<StringBuilder>()
				.registerCache("a", (String s) -> {
					contagemChaveA.compute(s, (__, v) -> v == null? 1: v + 1);
					return s.toUpperCase();
				})
				.registerCache("b", (String s, CycleCache c) -> {
					contagemChaveB.compute(s, (__, v) -> v == null? 1: v + 1);
					return s + c.getCache("a", s);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "1a3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "2b3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "1b3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "2b3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "2a3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "1a3");
					s.append(x);
				})
				.generateCycle();
		StringBuilder b = new StringBuilder();
		cycle.validateAndPersist(b);
		assertEquals("1A32b32B31b31B32b32B32A31A3", b.toString());
		Arrays.asList("1a3", "2a3", "2b3", "1b3").forEach(s -> assertEquals(1, (int) contagemChaveA.get(s)));
		Arrays.asList("2b3", "1b3").forEach(s -> assertEquals(1, (int) contagemChaveB.get(s)));
		assertEquals(4, contagemChaveA.size());
		assertEquals(2, contagemChaveB.size());
	}

	@Test
	public void cacheDoesNotRecompute2() {
		HashMap<String, Integer> contagemChaveA = new HashMap<>();
		HashMap<String, Integer> contagemChaveB = new HashMap<>();

		ValidationCycle<StringBuilder> cycle = new CycleBuilder<StringBuilder>()
				.registerCache("a", (String s) -> {
					contagemChaveA.compute(s, (__, v) -> v == null? 1: v + 1);
					return s.toUpperCase();
				})
				.registerCache("b", (String s, CycleCache c) -> {
					contagemChaveB.compute(s, (__, v) -> v == null? 1: v + 1);
					return s + c.getCache("a", s);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "1a3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "2b3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "2b3");
					s.append(x);
				})
				.generateCycle();
		StringBuilder b = new StringBuilder();
		cycle.validateAndPersist(b);
		assertEquals("1A32b32B32B3", b.toString());
		Arrays.asList("1a3", "2b3").forEach(s -> assertEquals(1, (int) contagemChaveA.get(s)));
		Arrays.asList("2b3").forEach(s -> assertEquals(1, (int) contagemChaveB.get(s)));
		assertEquals(2, contagemChaveA.size());
		assertEquals(1, contagemChaveB.size());
	}

	@Test
	public void cacheDoesNotRecomputeNull() {
		HashMap<String, Integer> contagemChaveA = new HashMap<>();
		HashMap<String, Integer> contagemChaveB = new HashMap<>();

		ValidationCycle<StringBuilder> cycle = new CycleBuilder<StringBuilder>()
				.registerCache("a", (String s) -> {
					contagemChaveA.compute(s, (__, v) -> v == null? 1: v + 1);
					return s.toUpperCase();
				})
				.registerCache("b", (String s, CycleCache c) -> {
					contagemChaveB.compute(s, (__, v) -> v == null? 1: v + 1);
					return null;
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "1a3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "2b3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "2b3");
					s.append(x);
				})
				.generateCycle();
		StringBuilder b = new StringBuilder();
		cycle.validateAndPersist(b);
		assertEquals("1A3nullnull", b.toString());
		Arrays.asList("1a3").forEach(s -> assertEquals(1, (int) contagemChaveA.get(s)));
		Arrays.asList("2b3").forEach(s -> assertEquals(1, (int) contagemChaveB.get(s)));
		assertEquals(1, contagemChaveA.size());
		assertEquals(1, contagemChaveB.size());
	}

	@Test(expected = ConcurrentModificationException.class)
	public void cacheRecursive() {
		new CycleBuilder<StringBuilder>()
				.registerCache("a", (String s) -> s.toUpperCase())
				.registerCache("b", (String s, CycleCache c) -> s + c.getCache("b", s))
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "1a3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "2b3");
					s.append(x);
				})
				.generateCycle().validateAndPersist(new StringBuilder());
	}

	@Test(expected = ConcurrentModificationException.class)
	public void cacheRecursiveIndirectly() {
		new CycleBuilder<StringBuilder>()
				.registerCache("a", (String s) -> s.toUpperCase())
				.registerCache("b", (String s, CycleCache c) -> s + c.getCache("c", s))
				.registerCache("c", (String s, CycleCache c) -> s + c.getCache("b", s))
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "1a3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "2b3");
					s.append(x);
				})
				.generateCycle().validateAndPersist(new StringBuilder());
	}

	@Test
	public void cacheDoesNotRecompute2IndependentCycles() {
		HashMap<String, Integer> contagemChaveA = new HashMap<>();
		HashMap<String, Integer> contagemChaveB = new HashMap<>();

		ValidationCycle<StringBuilder> cycle = new CycleBuilder<StringBuilder>()
				.registerCache("a", (String s) -> {
					contagemChaveA.compute(s, (__, v) -> v == null? 1: v + 1);
					return s.toUpperCase();
				})
				.registerCache("b", (String s, CycleCache c) -> {
					contagemChaveB.compute(s, (__, v) -> v == null? 1: v + 1);
					return s + c.getCache("a", s);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "1a3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("b", "2b3");
					s.append(x);
				})
				.addModifier(CycleBuilder.always(), (s, c) -> {
					String x = c.getCache("a", "2b3");
					s.append(x);
				})
				.generateCycle();
		StringBuilder b = new StringBuilder();
		cycle.validateAndPersist(b);

		StringBuilder c = new StringBuilder();
		cycle.validateAndPersist(c);

		assertEquals("1A32b32B32B3", b.toString());
		assertEquals("1A32b32B32B3", c.toString());
		Arrays.asList("1a3", "2b3").forEach(s -> assertEquals(2, (int) contagemChaveA.get(s)));
		Arrays.asList("2b3").forEach(s -> assertEquals(2, (int) contagemChaveB.get(s)));
		assertEquals(2, contagemChaveA.size());
		assertEquals(1, contagemChaveB.size());
	}
}

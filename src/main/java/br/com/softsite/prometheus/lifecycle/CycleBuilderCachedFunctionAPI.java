package br.com.softsite.prometheus.lifecycle;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import br.com.softsite.prometheus.entity.Doubt;
import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.entity.Inform;
import br.com.softsite.prometheus.lifecycle.testresult.*;

interface CycleBuilderCachedFunctionAPI<T> extends CycleBuilderSelf<T> {

	default CycleBuilder<T> addValidator(Predicate<T> imalive, BiFunction<T, CycleCache, Frankenstein> validator) {
		return getSelf().addValidator(TestAndFrankenstein.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromExplode(Predicate<T> imalive, BiFunction<T, CycleCache, Explode> validator) {
		return getSelf().addValidator(TestAndExplode.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromExplodes(Predicate<T> imalive, BiFunction<T, CycleCache, List<Explode>> validator) {
		return getSelf().addValidator(TestAndExplodes.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromDoubt(Predicate<T> imalive, BiFunction<T, CycleCache, Doubt> validator) {
		return getSelf().addValidator(TestAndDoubt.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromDoubts(Predicate<T> imalive, BiFunction<T, CycleCache, List<Doubt>> validator) {
		return getSelf().addValidator(TestAndDoubts.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromInform(Predicate<T> imalive, BiFunction<T, CycleCache, Inform> validator) {
		return getSelf().addValidator(TestAndInform.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromInforms(Predicate<T> imalive, BiFunction<T, CycleCache, List<Inform>> validator) {
		return getSelf().addValidator(TestAndInforms.basic(imalive, validator));
	}

	default CycleBuilder<T> addModifier(Predicate<T> imalive, BiConsumer<T, CycleCache> modifier) {
		return getSelf().addModifier(TestAndConsume.basic(imalive, modifier));
	}

	default CycleBuilder<T> addModifierFromInform(Predicate<T> imalive, BiFunction<T, CycleCache, Inform> modifier) {
		return getSelf().addModifier(TestAndInform.basic(imalive, modifier));
	}

	default CycleBuilder<T> addModifierFromInforms(Predicate<T> imalive, BiFunction<T, CycleCache, List<Inform>> modifier) {
		return getSelf().addModifier(TestAndInforms.basic(imalive, modifier));
	}

	default CycleBuilder<T> addPersister(Predicate<T> imalive, BiConsumer<T, CycleCache> persister) {
		return getSelf().addPersister(TestAndConsume.basic(imalive, persister));
	}

	default CycleBuilder<T> addPersisterFromInform(Predicate<T> imalive, BiFunction<T, CycleCache, Inform> persister) {
		return getSelf().addPersister(TestAndInform.basic(imalive, persister));
	}

	default CycleBuilder<T> addPersisterFromInforms(Predicate<T> imalive, BiFunction<T, CycleCache, List<Inform>> persister) {
		return getSelf().addPersister(TestAndInforms.basic(imalive, persister));
	}
}

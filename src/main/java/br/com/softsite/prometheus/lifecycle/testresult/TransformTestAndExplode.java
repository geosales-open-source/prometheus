package br.com.softsite.prometheus.lifecycle.testresult;

import java.util.function.Function;

public interface TransformTestAndExplode<T, C> {

	TestAndExplode<C> getTestTransformBase();
	Function<T, C> getTransformation();

	default TestAndExplode<T> transform() {
		return getTestTransformBase().transform(getTransformation());
	}

	static <T, C> TransformTestAndExplode<T, C> basic(Function<T, C> transform, TestAndExplode<C> test) {
		return new TransformTestAndExplode<T, C>() {

			@Override
			public Function<T, C> getTransformation() {
				return transform;
			}

			@Override
			public TestAndExplode<C> getTestTransformBase() {
				return test;
			}
		};
	}
}

package br.com.softsite.prometheus.lifecycle.testresult;

import java.util.function.Function;

public interface TransformTestAndDoubts<T, C> {

	TestAndDoubts<C> getTestTransformBase();
	Function<T, C> getTransformation();

	default TestAndDoubts<T> transform() {
		return getTestTransformBase().transform(getTransformation());
	}

	static <T, C> TransformTestAndDoubts<T, C> basic(Function<T, C> transform, TestAndDoubts<C> test) {
		return new TransformTestAndDoubts<T, C>() {

			@Override
			public Function<T, C> getTransformation() {
				return transform;
			}

			@Override
			public TestAndDoubts<C> getTestTransformBase() {
				return test;
			}
		};
	}
}

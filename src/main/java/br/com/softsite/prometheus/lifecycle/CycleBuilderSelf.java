package br.com.softsite.prometheus.lifecycle;

interface CycleBuilderSelf<T> {

	CycleBuilder<T> getSelf();
}

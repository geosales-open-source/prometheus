package br.com.softsite.prometheus.lifecycle.testresult;

import java.util.function.Function;

public interface TransformTestAndConsume<T, C> {

	TestAndConsume<C> getTestTransformBase();
	Function<T, C> getTransformation();

	default TestAndConsume<T> transform() {
		return getTestTransformBase().transform(getTransformation());
	}

	static <T, C> TransformTestAndConsume<T, C> basic(Function<T, C> transform, TestAndConsume<C> test) {
		return new TransformTestAndConsume<T, C>() {

			@Override
			public Function<T, C> getTransformation() {
				return transform;
			}

			@Override
			public TestAndConsume<C> getTestTransformBase() {
				return test;
			}
		};
	}
}

package br.com.softsite.prometheus.lifecycle.testresult;

import static br.com.softsite.prometheus.lifecycle.testresult.TestAndResult.singleToBi;

import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.lifecycle.CycleCache;
import br.com.softsite.prometheus.lifecycle.Frankenstein;

public interface TestAndConsume<T> extends TestAndResult<T> {

	void accept(T t, CycleCache cycleCache);

	default <U> TestAndConsume<U> transform(Function<U, T> transformer) {
		return basic((u, cc) -> this.test(transformer.apply(u), cc),
				(u, cc) -> this.accept(transformer.apply(u), cc));
	}

	@Override
	default Frankenstein apply(T t, CycleCache cycleCache) {
		accept(t, cycleCache);
		return Frankenstein.getEmpty();
	}

	static <T> TestAndConsume<T> basic(BiPredicate<T, CycleCache> imAlive, BiConsumer<T, CycleCache> accepter) {
		return new TestAndConsume<T>() {
			@Override
			public void accept(T t, CycleCache cycleCache) {
				accepter.accept(t, cycleCache);
			}

			@Override
			public boolean test(T t, CycleCache cycleCache) {
				return imAlive.test(t, cycleCache);
			}
		};
	}

	static <T> TestAndConsume<T> basic(BiPredicate<T, CycleCache> imAlive, Consumer<T> accepter) {
		return basic(imAlive, singleToBi(accepter));
	}

	static <T> TestAndConsume<T> basic(Predicate<T> imAlive, BiConsumer<T, CycleCache> accepter) {
		return basic(singleToBi(imAlive), accepter);
	}

	static <T> TestAndConsume<T> basic(Predicate<T> imAlive, Consumer<T> accepter) {
		return basic(singleToBi(imAlive), singleToBi(accepter));
	}
}

package br.com.softsite.prometheus.lifecycle;

import java.util.function.Function;

import br.com.softsite.prometheus.lifecycle.testresult.*;

interface CycleBuilderFunctionTransformAPI<T> extends CycleBuilderSelf<T> {

	default <C> CycleBuilder<T> addValidator(Function<T, C> transformer, TestAndFrankenstein<C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform(transformer));
	}

	default <C> CycleBuilder<T> addValidator(Function<T, C> transformer, TestAndExplode<C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform(transformer));
	}

	default <C> CycleBuilder<T> addValidator(Function<T, C> transformer, TestAndExplodes<C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform(transformer));
	}

	default <C> CycleBuilder<T> addValidator(Function<T, C> transformer, TestAndDoubt<C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform(transformer));
	}

	default <C> CycleBuilder<T> addValidator(Function<T, C> transformer, TestAndDoubts<C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform(transformer));
	}

	default <C> CycleBuilder<T> addValidator(Function<T, C> transformer, TestAndInform<C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform(transformer));
	}

	default <C> CycleBuilder<T> addValidator(Function<T, C> transformer, TestAndInforms<C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform(transformer));
	}

	default <C> CycleBuilder<T> addModifier(Function<T, C> transformer, TestAndConsume<C> imaliveModifier) {
		return getSelf().addModifier(imaliveModifier.transform(transformer));
	}

	default <C> CycleBuilder<T> addModifier(Function<T, C> transformer, TestAndInform<C> imaliveModifier) {
		return getSelf().addModifier(imaliveModifier.transform(transformer));
	}

	default <C> CycleBuilder<T> addModifier(Function<T, C> transformer, TestAndInforms<C> imaliveModifier) {
		return getSelf().addModifier(imaliveModifier.transform(transformer));
	}

	default <C> CycleBuilder<T> addPersister(Function<T, C> transformer, TestAndConsume<C> imalivePersister) {
		return getSelf().addPersister(imalivePersister.transform(transformer));
	}

	default <C> CycleBuilder<T> addPersister(Function<T, C> transformer, TestAndInform<C> imalivePersister) {
		return getSelf().addPersister(imalivePersister.transform(transformer));
	}

	default <C> CycleBuilder<T> addPersister(Function<T, C> transformer, TestAndInforms<C> imalivePersister) {
		return getSelf().addPersister(imalivePersister.transform(transformer));
	}
}

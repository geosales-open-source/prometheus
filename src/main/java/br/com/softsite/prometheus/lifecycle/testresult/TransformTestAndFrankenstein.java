package br.com.softsite.prometheus.lifecycle.testresult;

import java.util.function.Function;

public interface TransformTestAndFrankenstein<T, C> {

	TestAndFrankenstein<C> getTestTransformBase();
	Function<T, C> getTransformation();

	default TestAndFrankenstein<T> transform() {
		return getTestTransformBase().transform(getTransformation());
	}

	static <T, C> TransformTestAndFrankenstein<T, C> basic(Function<T, C> transform, TestAndFrankenstein<C> test) {
		return new TransformTestAndFrankenstein<T, C>() {

			@Override
			public Function<T, C> getTransformation() {
				return transform;
			}

			@Override
			public TestAndFrankenstein<C> getTestTransformBase() {
				return test;
			}
		};
	}
}

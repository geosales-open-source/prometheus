package br.com.softsite.prometheus.lifecycle.testresult;

import br.com.softsite.prometheus.lifecycle.CycleCache;
import br.com.softsite.prometheus.lifecycle.Frankenstein;

public interface TestAndX<T, X> extends TestAndResult<T> {

	X generateX(T t, CycleCache cc);
	Frankenstein xToFrank(X x);

	default Frankenstein apply(T t, CycleCache cc) {
		return xToFrank(generateX(t, cc));
	}
}

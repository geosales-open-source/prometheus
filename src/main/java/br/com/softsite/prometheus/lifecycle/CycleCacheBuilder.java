package br.com.softsite.prometheus.lifecycle;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import br.com.softsite.prometheus.MapDeffer;

class CycleCacheBuilder {

	private boolean built = false;
	private Map<String, BiFunction<Object, CycleCache, Object>> cacheFunctions = new HashMap<>();

	<T extends Object, K extends Object> void addCycleCache(String cacheId, Function<K, T> f) {
		addCycleCache(cacheId, (K k, CycleCache cc) -> f.apply(k));
	}

	<T extends Object, K extends Object> void addCycleCache(String cacheId, BiFunction<K, CycleCache, T> f) {
		if (cacheId == null) {
			throw new NullPointerException("cacheId não por ser nulo no Prometheus");
		}
		if (cacheFunctions.get(cacheId) != null) {
			throw new RuntimeException("Já tem neste ciclo um cache registrado para " + cacheId);
		}
		if (built) {
			built = false;
			cacheFunctions = new MapDeffer<>(cacheFunctions);
		}
		cacheFunctions.put(cacheId, (BiFunction) f);
	}

	CycleCache getCycleCache() {
		if (cacheFunctions.size() == 0) {
			return CycleCacheEmpty.INSTANCE;
		}
		Map<String, BiFunction<Object, CycleCache, Object>> cacheFunctions = this.cacheFunctions;
		built = true;
		return new CycleCacheImpl(cacheFunctions);
	}
}

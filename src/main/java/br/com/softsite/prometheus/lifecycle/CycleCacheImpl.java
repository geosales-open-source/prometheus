package br.com.softsite.prometheus.lifecycle;

import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import br.com.softsite.toolbox.Pair;

class CycleCacheImpl implements CycleCache {

	final transient HashMap<Pair<String, Object>, Object> cache = new HashMap<>();
	final Function<String, BiFunction<Object, CycleCache, Object>> allCaches;
	private static final Object NULL_GUARDIAN = new Object();
	private static final Object RECURSIVE_GUARDIAN = new Object();

	CycleCacheImpl(Map<String, BiFunction<Object, CycleCache, Object>> cacheFunctions) {
		if (cacheFunctions != null) {
			allCaches = cacheFunctions::get;
		} else {
			allCaches = cId -> (k, cc) -> null;
		}
	}

	public <T, K> T getCache(String cacheId, K key) {
		if (cacheId == null) {
			throw new NullPointerException("cacheId não por ser nulo no Prometheus");
		}
		Pair<String, Object> acesso = Pair.getPair(cacheId, key);
		T v = (T) cache.get(acesso);
		if (v == RECURSIVE_GUARDIAN) {
			throw new ConcurrentModificationException("cache não pode depender da mesma chave recursivamente");
		}
		if (v == NULL_GUARDIAN) {
			return null;
		}
		if (v != null) {
			return v;
		}
		cache.put(acesso, RECURSIVE_GUARDIAN);
		v = (T) Optional.of(cacheId)
			.map(allCaches::apply)
			.map(f -> f.apply(key, this))
			.orElse(null);
		if (v == null) {
			cache.put(acesso, NULL_GUARDIAN);
		} else {
			cache.put(acesso, v);
		}
		return v;
	}
}

package br.com.softsite.prometheus.lifecycle.testresult;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.lifecycle.CycleCache;
import br.com.softsite.prometheus.lifecycle.Frankenstein;

public interface TestAndResult<T> {

	Frankenstein apply(T t, CycleCache cycleCache);
	boolean test(T t, CycleCache cycleCache);

	static <T, U, R> BiFunction<T, U, R> singleToBi(Function<T, R> f) {
		return (t, __) -> f.apply(t);
	}

	static <T, U> BiConsumer<T, U> singleToBi(Consumer<T> c) {
		return (t, __) -> c.accept(t);
	}

	static <T, U> BiPredicate<T, U> singleToBi(Predicate<T> p) {
		return (t, __) -> p.test(t);
	}
}

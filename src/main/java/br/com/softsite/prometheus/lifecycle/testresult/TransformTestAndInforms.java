package br.com.softsite.prometheus.lifecycle.testresult;

import java.util.function.Function;

public interface TransformTestAndInforms<T, C> {

	TestAndInforms<C> getTestTransformBase();
	Function<T, C> getTransformation();

	default TestAndInforms<T> transform() {
		return getTestTransformBase().transform(getTransformation());
	}

	static <T, C> TransformTestAndInforms<T, C> basic(Function<T, C> transform, TestAndInforms<C> test) {
		return new TransformTestAndInforms<T, C>() {

			@Override
			public Function<T, C> getTransformation() {
				return transform;
			}

			@Override
			public TestAndInforms<C> getTestTransformBase() {
				return test;
			}
		};
	}
}

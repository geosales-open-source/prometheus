package br.com.softsite.prometheus.lifecycle.testresult;

import java.util.function.Function;

public interface TransformTestAndInform<T, C> {

	TestAndInform<C> getTestTransformBase();
	Function<T, C> getTransformation();

	default TestAndInform<T> transform() {
		return getTestTransformBase().transform(getTransformation());
	}

	static <T, C> TransformTestAndInform<T, C> basic(Function<T, C> transform, TestAndInform<C> test) {
		return new TransformTestAndInform<T, C>() {

			@Override
			public Function<T, C> getTransformation() {
				return transform;
			}

			@Override
			public TestAndInform<C> getTestTransformBase() {
				return test;
			}
		};
	}
}

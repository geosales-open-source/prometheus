package br.com.softsite.prometheus.lifecycle;

import br.com.softsite.prometheus.lifecycle.testresult.*;

interface CycleBuilderFunctionAPI<T> extends CycleBuilderSelf<T> {

	default CycleBuilder<T> addValidator(TestAndFrankenstein<T> imaliveValidator) {
		getSelf().validators().add(imaliveValidator);
		return getSelf();
	}

	default CycleBuilder<T> addValidator(TestAndExplode<T> imaliveValidator) {
		getSelf().validators().add(imaliveValidator);
		return getSelf();
	}

	default CycleBuilder<T> addValidator(TestAndExplodes<T> imaliveValidator) {
		getSelf().validators().add(imaliveValidator);
		return getSelf();
	}

	default CycleBuilder<T> addValidator(TestAndDoubt<T> imaliveValidator) {
		getSelf().validators().add(imaliveValidator);
		return getSelf();
	}

	default CycleBuilder<T> addValidator(TestAndDoubts<T> imaliveValidator) {
		getSelf().validators().add(imaliveValidator);
		return getSelf();
	}

	default CycleBuilder<T> addValidator(TestAndInform<T> imaliveValidator) {
		getSelf().validators().add(imaliveValidator);
		return getSelf();
	}

	default CycleBuilder<T> addValidator(TestAndInforms<T> imaliveValidator) {
		getSelf().validators().add(imaliveValidator);
		return getSelf();
	}

	default CycleBuilder<T> addModifier(TestAndConsume<T> imaliveModifier) {
		getSelf().modifiers().add(imaliveModifier);
		return getSelf();
	}

	default CycleBuilder<T> addModifier(TestAndInform<T> imaliveModifier) {
		getSelf().modifiers().add(imaliveModifier);
		return getSelf();
	}

	default CycleBuilder<T> addModifier(TestAndInforms<T> imaliveModifier) {
		getSelf().modifiers().add(imaliveModifier);
		return getSelf();
	}

	default CycleBuilder<T> addPersister(TestAndConsume<T> imalivePersister) {
		getSelf().persisters().add(imalivePersister);
		return getSelf();
	}

	default CycleBuilder<T> addPersister(TestAndInform<T> imalivePersister) {
		getSelf().persisters().add(imalivePersister);
		return getSelf();
	}

	default CycleBuilder<T> addPersister(TestAndInforms<T> imalivePersister) {
		getSelf().persisters().add(imalivePersister);
		return getSelf();
	}
}

package br.com.softsite.prometheus.lifecycle.testresult;

import static br.com.softsite.prometheus.lifecycle.testresult.TestAndResult.singleToBi;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.entity.Doubt;
import br.com.softsite.prometheus.lifecycle.CycleCache;
import br.com.softsite.prometheus.lifecycle.Frankenstein;

public interface TestAndDoubt<T> extends TestAndX<T, Doubt> {

	Doubt generateX(T t, CycleCache cc);

	default <U> TestAndDoubt<U> transform(Function<U, T> transformer) {
		return basic((u, cc) -> this.test(transformer.apply(u), cc),
				(u, cc) -> this.generateX(transformer.apply(u), cc));
	}

	@Override
	default Frankenstein xToFrank(Doubt doubt) {
		return Frankenstein.fromDoubt(doubt);
	}

	static <T> TestAndDoubt<T> basic(BiPredicate<T, CycleCache> imAlive, BiFunction<T, CycleCache, Doubt> f) {
		return new TestAndDoubt<T>() {

			@Override
			public Doubt generateX(T t, CycleCache cycleCache) {
				return f.apply(t, cycleCache);
			}

			@Override
			public boolean test(T t, CycleCache cycleCache) {
				return imAlive.test(t, cycleCache);
			}
		};
	}

	static <T> TestAndDoubt<T> basic(BiPredicate<T, CycleCache> imAlive, Function<T, Doubt> f) {
		return basic(imAlive, singleToBi(f));
	}

	static <T> TestAndDoubt<T> basic(Predicate<T> imAlive, BiFunction<T, CycleCache, Doubt> f) {
		return basic(singleToBi(imAlive), f);
	}

	static <T> TestAndDoubt<T> basic(Predicate<T> imAlive, Function<T, Doubt> f) {
		return basic(singleToBi(imAlive), singleToBi(f));
	}
}

package br.com.softsite.prometheus.lifecycle.testresult;

import java.util.function.Function;

public interface TransformTestAndExplodes<T, C> {

	TestAndExplodes<C> getTestTransformBase();
	Function<T, C> getTransformation();

	default TestAndExplodes<T> transform() {
		return getTestTransformBase().transform(getTransformation());
	}

	static <T, C> TransformTestAndExplodes<T, C> basic(Function<T, C> transform, TestAndExplodes<C> test) {
		return new TransformTestAndExplodes<T, C>() {

			@Override
			public Function<T, C> getTransformation() {
				return transform;
			}

			@Override
			public TestAndExplodes<C> getTestTransformBase() {
				return test;
			}
		};
	}
}

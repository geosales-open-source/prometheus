package br.com.softsite.prometheus.lifecycle;

import br.com.softsite.prometheus.lifecycle.testresult.*;

interface CycleBuilderTransformAPI<T> extends CycleBuilderSelf<T> {

	default <C> CycleBuilder<T> addValidator(TransformTestAndFrankenstein<T, C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform());
	}

	default <C> CycleBuilder<T> addValidator(TransformTestAndExplode<T, C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform());
	}

	default <C> CycleBuilder<T> addValidator(TransformTestAndExplodes<T, C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform());
	}

	default <C> CycleBuilder<T> addValidator(TransformTestAndDoubt<T, C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform());
	}

	default <C> CycleBuilder<T> addValidator(TransformTestAndDoubts<T, C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform());
	}

	default <C> CycleBuilder<T> addValidator(TransformTestAndInform<T, C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform());
	}

	default <C> CycleBuilder<T> addValidator(TransformTestAndInforms<T, C> imaliveValidator) {
		return getSelf().addValidator(imaliveValidator.transform());
	}

	default <C> CycleBuilder<T> addModifier(TransformTestAndConsume<T, C> imaliveModifier) {
		return getSelf().addModifier(imaliveModifier.transform());
	}

	default <C> CycleBuilder<T> addModifier(TransformTestAndInform<T, C> imaliveModifier) {
		return getSelf().addModifier(imaliveModifier.transform());
	}

	default <C> CycleBuilder<T> addModifier(TransformTestAndInforms<T, C> imaliveModifier) {
		return getSelf().addModifier(imaliveModifier.transform());
	}

	default <C> CycleBuilder<T> addPersister(TransformTestAndConsume<T, C> imalivePersister) {
		return getSelf().addPersister(imalivePersister.transform());
	}

	default <C> CycleBuilder<T> addPersister(TransformTestAndInform<T, C> imalivePersister) {
		return getSelf().addPersister(imalivePersister.transform());
	}

	default <C> CycleBuilder<T> addPersister(TransformTestAndInforms<T, C> imalivePersister) {
		return getSelf().addPersister(imalivePersister.transform());
	}
}

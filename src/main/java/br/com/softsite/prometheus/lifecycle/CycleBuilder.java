package br.com.softsite.prometheus.lifecycle;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.lifecycle.testresult.TestAndResult;

public class CycleBuilder<T> implements CycleBuilderSelf<T>,
		CycleBuilderFunctionAPI<T>,
		CycleBuilderFunctionTransformAPI<T>,

		CycleBuilderBaseAPI<T>,
		CycleBuilderCachedImAliveAPI<T>,
		CycleBuilderCachedFunctionAPI<T>,
		CycleBuilderCachedFullAPI<T>,

		CycleBuilderTransformFullBaseAPI<T>,
		CycleBuilderTransformFullCachedImAliveAPI<T>,
		CycleBuilderTransformFullCachedFunctionAPI<T>,
		CycleBuilderTransformFullCachedFullAPI<T>,

		CycleBuilderTransformAPI<T> {

	private static final Predicate<Object> ALWAYS = __ -> true;
	private static final BiPredicate<Object, CycleCache> ALWAYS2 = (__, ___) -> true;

	public static <X> Predicate<X> always() {
		return (Predicate<X>) ALWAYS;
	}

	public static <X> BiPredicate<X, CycleCache> always2() {
		return (BiPredicate<X, CycleCache>) ALWAYS2;
	}

	@Override
	public CycleBuilder<T> getSelf() {
		return this;
	}

	private List<TestAndResult<T>> validators = new ArrayList<>();
	private List<TestAndResult<T>> modifiers = new ArrayList<>();
	private List<TestAndResult<T>> persisters = new ArrayList<>();
	private Consumer<Runnable> runInTransaction = Runnable::run;
	private CycleCacheBuilder cycleCacheBuilder = new CycleCacheBuilder();

	public CycleBuilder() {
	}

	List<TestAndResult<T>> validators() {
		return validators;
	}

	List<TestAndResult<T>> modifiers() {
		return modifiers;
	}

	List<TestAndResult<T>> persisters() {
		return persisters;
	}

	public CycleBuilder<T> setRunInTransaction(Consumer<Runnable> runInTransaction) {
		if (runInTransaction == null) {
			throw new NullPointerException("runInTransaction não pode ser nulo, o padrão é Runnable::run");
		}
		this.runInTransaction = runInTransaction;
		return this;
	}

	public ValidationCycle<T> generateCycle() {
		ValidationCycle<T> cycle = new ValidationCycle<>(toPipeline(this.validators), toPipeline(this.modifiers), toPipeline(this.persisters), runInTransaction, cycleCacheBuilder::getCycleCache);
		return cycle;
	}

	private Pipeline<T> toPipeline(List<TestAndResult<T>> almostPipe) {
		return Pipeline.build(almostPipe);
	}

	// register-cache-api

	public <K, V> CycleBuilder<T> registerCache(String cacheId, Function<K, V> cacheGetter) {
		cycleCacheBuilder.addCycleCache(cacheId, cacheGetter);
		return this;
	}

	public <K, V> CycleBuilder<T> registerCache(String cacheId, BiFunction<K, CycleCache, V> cacheGetter) {
		cycleCacheBuilder.addCycleCache(cacheId, cacheGetter);
		return this;
	}
}

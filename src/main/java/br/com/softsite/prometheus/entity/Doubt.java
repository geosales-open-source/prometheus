package br.com.softsite.prometheus.entity;

import br.com.softsite.toolbox.IsSerializable;

public class Doubt implements IsSerializable {

	private String message;

	Doubt() {
	}

	public Doubt(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}
}
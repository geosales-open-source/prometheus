package br.com.softsite.prometheus.entity;

import java.util.ArrayList;
import java.util.List;

import br.com.softsite.toolbox.IsSerializable;

public class PrometheusDTO<T> implements IsSerializable {
	private T obj;
	private List<Explode> explodes = new ArrayList<>();
	private List<Doubt> doubts = new ArrayList<>();
	private List<Inform> informs = new ArrayList<>();

	public List<Explode> getExplodes() {
		return explodes;
	}

	public List<Doubt> getDoubts() {
		return doubts;
	}

	public List<Inform> getInforms() {
		return informs;
	}

	public PrometheusDTO() {
	}

	public PrometheusDTO(T obj) {
		this();
		this.obj = obj;
	}

	public void setObject(T obj) {
		this.obj = obj;
	}

	public T getObj() {
		return this.obj;
	}

	public void addExplode(Explode explode) {
		this.explodes.add(explode);
	}

	public void addDoubt(Doubt doubt) {
		this.doubts.add(doubt);
	}

	public void addInform(Inform inform) {
		this.informs.add(inform);
	}

	public void addExplodes(List<Explode> explodes) {
		this.explodes.addAll(explodes);
	}

	public void addDoubts(List<Doubt> doubts) {
		this.doubts.addAll(doubts);
	}

	public void addInforms(List<Inform> informs) {
		this.informs.addAll(informs);
	}

	public boolean allOk() {
		return doubts.isEmpty() && explodes.isEmpty();
	}

	public boolean hasExplodes() {
		return !this.explodes.isEmpty();
	}

	public boolean hasExplodesOfType(String explodeType) {
		if (explodeType == null || "".equals(explodeType)) {
			return false;
		}
		return (hasExplodes() && getExplodes().stream().anyMatch(e -> explodeType.equals(e.getExplodeType())));
	}

	public boolean hasInforms() {
		return !this.informs.isEmpty();
	}

	public boolean hasDoubts() {
		return !this.doubts.isEmpty();
	}

	public PrometheusDTO<T> merge(PrometheusDTO<T> another) {
		if (this != another) {
			this.doubts.addAll(another.doubts);
			this.explodes.addAll(another.explodes);
			this.informs.addAll(another.informs);
		}
		
		return this;
	}
}
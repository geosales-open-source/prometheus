package br.com.softsite.prometheus.entity;

import br.com.softsite.toolbox.IsSerializable;

public class Inform implements IsSerializable {

	private String message;

	Inform() {
	}

	public Inform(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	@Override
	public String toString() {
		return getMessage();
	}
}
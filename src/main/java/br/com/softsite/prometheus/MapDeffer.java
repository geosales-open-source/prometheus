package br.com.softsite.prometheus;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapDeffer<K, V> implements Map<K, V> {

	private Map<K, V> original;
	private Map<K, V> newMap;
	private transient Set<Entry<K, V>> setEntry;

	public MapDeffer(Map<K, V> original) {
		this(original, new HashMap<>());
	}

	public MapDeffer(Map<K, V> original, Map<K, V> newMap) {
		this.original = original;
		this.newMap = newMap;
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("cannot clear MapDeffer");
	}

	@Override
	public boolean containsKey(Object key) {
		return newMap.containsKey(key) || original.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return newMap.containsValue(value) || original.containsValue(value);
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		if (setEntry != null) {
			return setEntry;
		}
		return setEntry = new Set<Entry<K, V>>() {
			Set<Entry<K, V>> defferedEntrySet = original.entrySet();
			Set<Entry<K, V>> newSet = newMap.entrySet();

			@Override
			public boolean add(Entry<K, V> kvEntry) {
				throw new UnsupportedOperationException();
			}

			@Override
			public boolean addAll(Collection<? extends Entry<K, V>> c) {
				throw new UnsupportedOperationException();
			}

			@Override
			public void clear() {
				throw new UnsupportedOperationException();
			}

			@Override
			public boolean contains(Object o) {
				return defferedEntrySet.contains(o) || newSet.contains(o);
			}

			@Override
			public boolean containsAll(Collection<?> c) {
				return c.stream().allMatch(this::contains);
			}

			@Override
			public boolean equals(Object o) {
				if (this == o) {
					return true;
				} else if (o == null) {
					return false;
				} else if (!(o instanceof Set)) {
					return false;
				}
				@SuppressWarnings("unchecked")
				Set<Entry<K, V>> otherMyClass = (Set<Entry<K,V>>) o;

				return containsAll(otherMyClass) && otherMyClass.containsAll(this);
			}

			@Override
			public int hashCode() {
				return defferedEntrySet.hashCode() + newSet.hashCode();
			}

			@Override
			public boolean isEmpty() {
				return defferedEntrySet.isEmpty() && newSet.isEmpty();
			}

			@Override
			public Iterator<Entry<K, V>> iterator() {

				return new Iterator<Entry<K, V>>() {
					private Iterator<Entry<K, V>> newSetIteraror = newSet.iterator();
					private Iterator<Entry<K, V>> defferedIterator = defferedEntrySet.iterator();

					private boolean hasEnteredDeffered = !newSetIteraror.hasNext();

					@Override
					public boolean hasNext() {
						return hasEnteredDeffered? defferedIterator.hasNext(): newSetIteraror.hasNext();
					}

					@Override
					public Entry<K, V> next() {
						if (hasEnteredDeffered) {
							return new EntryDeffer<>(defferedIterator.next());
						} else {
							Entry<K, V> e = newSetIteraror.next();
							if (!newSetIteraror.hasNext()) {
								hasEnteredDeffered = true;
							}
							return e;
						}
					}

					@Override
					public void remove() {
						if (hasEnteredDeffered) {
							throw new UnsupportedOperationException("cannot modify deffered map from MapDeffer");
						}
						newSetIteraror.remove();
					}
				};
			}

			@Override
			public boolean remove(Object o) {
				if (newSet.remove(o)) {
					return true;
				}
				if (defferedEntrySet.contains(o)) {
					throw new UnsupportedOperationException("cannot modify deffered map from MapDeffer");
				}

				return false;
			}

			@Override
			public boolean removeAll(Collection<?> c) {
				boolean anyFromDeffered = c.stream().anyMatch(defferedEntrySet::contains);
				if (anyFromDeffered) {
					throw new UnsupportedOperationException("cannot modify deffered map from MapDeffer");
				}
				return newSet.removeAll(c);
			}

			@Override
			public boolean retainAll(Collection<?> c) {
				if (!c.containsAll(defferedEntrySet)) {
					throw new UnsupportedOperationException("cannot modify deffered map from MapDeffer");
				}
				return newSet.retainAll(c);
			}

			@Override
			public int size() {
				return newSet.size() + defferedEntrySet.size();
			}

			@Override
			public Object[] toArray() {
				Object[] array = new Object[size()];
				int i = 0;
				for (Entry<K, V> e: this) {
					array[i++] = e;
				}
				return array;
			}

			@Override
			@SuppressWarnings("unchecked")
			public <T> T[] toArray(T[] a) {
				int mySize = size();
				if (a.length < mySize) {
					return Stream.concat(newSet.stream(), defferedEntrySet.stream()).toArray(l -> (T[]) Array.newInstance(a.getClass().getComponentType(), l));
				}

				int p = 0;
				for (Entry<K, V> e: newSet) {
					a[p++] = (T) e;
				}
				for (Entry<K, V> e: defferedEntrySet) {
					a[p++] = (T) e;
				}
				if (a.length > mySize) {
					a[mySize] = null;
				}
				return a;
			}
		};
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (!(o instanceof Map)) {
			return false;
		}
		@SuppressWarnings("unchecked")
		Map<K, V> otherMap = (Map<K, V>) o;
		return entrySet().equals(otherMap.entrySet());
	}

	@Override
	public V get(Object key) {
		V v = newMap.get(key);
		if (v != null) {
			return v;
		}
		if (newMap.containsKey(key)) {
			return null;
		}
		return original.get(key);
	}

	@Override
	public int hashCode() {
		return entrySet().hashCode();
	}

	@Override
	public boolean isEmpty() {
		return newMap.isEmpty() && original.isEmpty();
	}

	@Override
	public Set<K> keySet() {
		return concatCollection(original.keySet(), newMap.keySet(), Collectors.toSet());
	}

	@Override
	public V put(K key, V value) {
		if (original.containsKey(key)) {
			throw new UnsupportedOperationException("cannot modify deffered map from MapDeffer");
		}
		return newMap.put(key, value);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		for (Entry<? extends K, ? extends V> e: m.entrySet()) {
			put(e.getKey(), e.getValue());
		}
	}

	@Override
	public V remove(Object key) {
		if (original.containsKey(key)) {
			throw new UnsupportedOperationException("cannot modify deffered map from MapDeffer");
		}
		return newMap.remove(key);
	}

	@Override
	public int size() {
		return newMap.size() + original.size();
	}

	@Override
	public Collection<V> values() {
		return concatCollection(original.values(), newMap.values(), Collectors.toList());
	}

	private static <E, C extends Collection<E>> C concatCollection(Collection<E> c1, Collection<E> c2, Collector<E, ?, C> collector) {
		return Stream.concat(c1.stream(), c2.stream()).collect(collector);
	}

	private static class EntryDeffer<K, V> implements Entry<K, V> {

		private Entry<K, V> original;
		private EntryDeffer(Entry<K, V> original) {
			if (original instanceof EntryDeffer) {
				this.original = ((EntryDeffer<K, V>) original).original;
			} else {
				this.original = original;
			}
		}

		@Override
		public int hashCode() {
			return original.hashCode();
		}

		@Override
		public boolean equals(Object o) {
			return original.equals(o);
		}

		@Override
		public K getKey() {
			return original.getKey();
		}

		@Override
		public V getValue() {
			return original.getValue();
		}

		@Override
		public V setValue(V value) {
			throw new UnsupportedOperationException("cannot modify deffered map from MapDeffer");
		}
	}
}
